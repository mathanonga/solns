# Trigonometry.
Sa madaling salita, polygon na tatlo ang sukat.

Kung bibilangin ang lahat ng uri ng tatsulok di tayo matatapos. Dito natin gagamitin ang Ratio and Proportion.

Similar Triangles, ang ratio ng mga corresponding sides ay nananatili, kumbaga sa larawan, parang zoom in zoom out lang. Ganun din naman, kahit na di triangles. Sa similar shapes: kung ang ratio ng corresponding side ratio ay $`p:q`$, ang magiging area ratio ay $`p^2:q^2`$ at ang volume ratio ay $`p^3:q^3`$ gaya ng nasa larawan. 

![](./trig/similarShapes.png) 

Sa parehong paraan, ganito gumagana ang Trigonometry. kung saan ang ratio ay ang radius ng circle na siya ring hypothenuse ng right triangle. na nabubuo ng angle  $`\theta`$.

Sa larawan sa itaas ay di magagamit ang trigonometry dahil, maraming kurbang linya. Ang trigonometry ay magagamit kung lahat ng guhit ay tuwid o kung may hindi man ay bilog o bahagi nito.

##### Paghahati
![galing sa https://brilliant.org/wiki/irregular-polygons/](./trig/triagon.png  "galing sa https://brilliant.org/wiki/irregular-polygons/")

Basta lahat ng linya ay tuwid, maaaring hatiin sa mga tatsulok.

![](./trig/triagons.png) 

Makikita sa saranggola o _kite_ na may manipis na linya. Ang mas makapal na linya ay mas maiging hati, upang magkauri ang mga tatsulok, one time solving na lang, kaysa hatiin sa manipis na linya, dalawang uri ng triangle.

Mas makikita ito sa hexagon, kung saan hinati ito sa anim na magkakasukat na tatsukat (hahaha joke, equilateral triangles).

##### Right angles
![](./trig/triagonsr.png) 

Upang magamit ang trigonometry. Maaaring hatiin pang muli upang magkaroon na ng pagkakakilanlan ang mga tatsulok. Madalas na sinusukat ay isang angulo liban sa right, at ang mga hypothenuse.
Mula sa dalawang sukat na ito ay matutukoy na ang lahat ng linya, maging ang kanilang mga sukat
>1. Sa unang hugis ay may siyam na right triangle. Maaaring sukatin ang mga anggulo mula sa gitnang vertex at pangalangang alpha, beta, gamma, delta, epsilon, zeta, eta, theta, at iota at ang kanilang mga hypothenuse na, a, b, c, d, e, f, g, h, at i ang mga sides nito ay matatawag at masusukat na 
sin alpha, sin beta + sin gamma, 
sin delta + sin epsilon, sin zeta + sin eta, 
sin theta + sin iota, cos iota, at cos alpha.
>2. Nahati sa apat na tatsulok ang _kite_ o dalawang pares na magkasukat na triangles.
>3. Ang huli ay isang uri lang ng triangle, labindalawang 30-60-90 degree triangle, na may ratio na $`1:\sqrt{3}:2`$ side ratio.