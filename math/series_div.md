```math
a_n = \frac{3^n + (-2)^n}{n}
```

If n is odd,

```math
3^n + (-2)^n = 3^n-2^n = (3-2)(3^{(n-1)}+3^{(n-2)}\cdot2+\cdots+3\cdot2^{(n-2)}+2^{(n-1)})
```
$`n`$ addends, each $`> 1`$

```math
\Rightarrow a_n > 1
```
else if $`n`$ is even,

```math
\Rightarrow 3^n > n \; \& \; (-2)^n = 2^n > n
```

```math
\Rightarrow 3^n +(-2)^n > 2n > n
\Rightarrow a_n > 1
```

```math
\Rightarrow \lim |a_n| \neq 0
```
Therefore, $`a_n`$ divergent
